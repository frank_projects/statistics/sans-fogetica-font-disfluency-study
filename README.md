# Sans Forgetica Font Disfluency Study

## Background  
Sans Forgetica is a special font type that is said to increase reading difficulty at an optimal level so that reading in that font increases retention of information. In other words some suggest that this special font can help people learn better.  

The claim is that the font forces readers to slow down and thus engage in deeper learning strategies. More information can be found at: https://sansforgetica.rmit/

![alt text](sanforgetica.JPG "Boxplot looking at the data")

## Objective of this study  
The objective of this study is to practice statistical methods thus I recommend readers take the results with a grain of salt and to consider consulting other research before coming to a conclusion for Sans Forgetica. 

Also do note that this was written April 9th, 2019. My skills as a statistician would have improved by the time you are viewing this.

We will try to answer the question whether Sans Forgetica increases learning disfluency.

## Intellectual Property 
The intellectual property of this research belongs to Dr. Jennifer Peters at Western of Western Ontario as part of an assignment for the course STATS2244 

## Study Setup
##### Population of interest
higher education students
##### Sampling frame
Students registered in Biology/Statistics 2244B (Winter term 2019) at Western University.
##### Sampling strategy
Email invitations to participate in the Data collection 1 and 2 Activities as part of a component of the course; students obtained points towards a course component for completion of the survey (with any answer). The invitation was sent through the course site for Biology/Statistics 2244B to all registered students in the course.
##### Study design
The data were collected in two stages.
Stage 1: An initial survey administered through the course website.
Stage 2: A repeated measures experiment in which participants read a text and answered some questions about the text. The text was split into two (approximately equal) halves representing the two treatments each student experienced in the study. One half was written in Times New Roman for all students. The other half of the text was written in one of two fonts designed to either increase (“Sans Forgetica” font) or decrease (“OpenDyslexia”) disfluency; which of these two fonts a student received was randomly determined. In addition, which half of the text was Times New Roman versus the other font was randomly determined for each student (i.e. randomization of order of treatment).

![alt text](studydesign.png "Boxplot looking at the data")

##### Measurement Descriptions
Data for several variables were collected across two different surveys. Refer back to the data collection questionnaires for the questions and possible answer values. A separate file provides some more information about some of the measurements, and identifies the variable names in the data file.

## Variables Considered
##### Elapsed Time
According to the research provided the longer an individual spends to read the text the more likely they are to be engaging in deep learning stratergies. Disfluency in the text would make reading it more difficult and thus would increase the time it takes to read it. Thus the difference in elapsed time will tell us whether Sans Forgetica increase disfluency compared to other fonts.

##### The Fonts Used
We will want to group the elapsed time into two categories. The first with users who had Sans Forgetica in at least one fo the segements of the reading. The second is the subset of individuals who did not receive any Sans Forgetica.

## Data Cleaning
##### Dealing with missing resilience questions
Since there are 3 possible missing values the best approach to demonstrating why it’s reasonable to assume that it’s missing completely at random is to disprove the other two types. 

Why it’s not missing not at random: If this was the case then it means that there is some factor that increases the tendency for the value to be missing. The value missing is from participant 3 whose data does not show any signs that there was some bias. The question itself is very similar to res11 which the participant answered. 

Why it’s not missing at random: If this was the case then there is a systematic reason for why the value is missing. We would expect then that perhaps individuals with certain backgrounds or traits be more likely to not fill in any answer. But if that were the case then we would expect more than 1 individual to feel compelled to not answer it. Thus, since we have ruled out the other two options it could only reasonably be missing completely at random.

Thus we will apply imputation for the resilience questions that are missing by using the value provided by it's complimentary question. Since for each question about resilience there is a complementary question that is reversed coded all we need to do is to imputated the missing data with the reverse code score of it's complement.

##### Dealing with individuals who indicated they are distracted
 Without changing the confidence level and sample size the only way to increase precision is by reducing the variation. My choice of removing distracted individuals is based on the idea that individuals who are distracted can cause a high level of varaition within the datat since there is a large range of distractions all of which will influence the time elapsed of each individual differently.
## Visualizing the data
![alt text](mosiacplot.JPG "Boxplot looking at the data")

![alt text](boxplot.JPG "Boxplot looking at the data")


## T-Test Difference in Means
For this research I will use the t-test for difference in population means since we will have the mean duration time of two different groups.

We know that:
1. The sampling strategy collected participants from the sampling frame of two Western university classes so in theory it's not a true SRS. It can be said though that the participants were assigned randomly to different treatment groups. For the purpose of this project however we will assume that the experiment is SRS.
2.  Independent samples from X1 ~ N (u1, SD1) and x2 ~ N(u2,SD2). Our sample size n is sufficiently large so that this condition is satisfied

##### Null hypothesis
Sentence form: There is no difference in the mean duration time for those who reads at least one passage in Sans Forgetica when compared to the mean duration time for those who reads the same passages in other fonts. Symbolic notation:u1 - u2 = 0
u1 - u2 = 0
##### Alternative hypothesis
Sentence form: There is a difference in the mean duration time for those who reads passages in Sans Forgetica when compared to the mean duration time for those who reads the same passages in other fonts. 
Symbolic notation: u2 - u1 > 0

##### T-Test Results
There is extremely strong indication that our null hypothesis holds (t = -1.4694, df = 50.194, p-value = 0.926), i.e. that there is no significant difference in the duration time for the population who has Sans Forgetica (M=7.6932, sd = 6.8955, n=99)  and those who were given other fonts  (M=6.4167, sd = 3.2384, n=84). Thus if learning disfluency increases the duration time we found that Sans Forgetica does not increase learning disfluency.


## Conclusion
Our experiment seeks to investigate whether reading Sans Forgetica increases disfluency compared to other fonts in order to investigate the claim that Sans Forgetica can force readers to engage in deeper learning stratergies and retain more information. Helin et al. found in their studies that reading time for font that is more difficult to comprehend would take a longer time and that particpants given difficult fonts retained information better.

We measured this by comparing the mean duration time of those who were given Sans Forgetica compared with those who were other fonts to read. The idea is that if Sans Forgetica increase the disfluency of readers then the mean duration time of participants who were given passages with this font will be significantly larger than the duration of participants who were given the same passages to read in other fonts. Our findings indicate that our sample did not find a significant difference in mean duration time for Sans Forgetica compared to other fonts. In fact it was found that the average duration of Sans Forgetica was actually less than the average duration of other font groups. In other words this experiment suggests that Sans Forgetica does not increase learning disfluency. 

This is in line with the findings from Eitel and Kühl measured learning times by conducting experiments where participants were given the same passages to read and given questions to test their knowledge. They varied the participants into different experimental blocks where they were given different types of font some of which are less legible. Their experiment found that the time duration did not signficiantly differ despite some fonts being more difficult.  Further more our data considered only individuals who answered the knowledge question correctly so it's unlikely that those in the Sans Forgetica group gave up on comprehending the material. 

There is a possible objection that the participants may have chose to give up on carefully reading the text and thus have a much lower duration time than the actual time required to read the text. Essentially it's possible that participants gave up halfway rendering the data in our study biased. However since we controlled for resiliency and only used participants with higher or moderate resilience we know that it's unlikely students gave up. This is because studies on reslience found that those who score on resilience are less likely to give up on tasks.We plan to continue investating the result here to try to reproduce the result that Sans Forgetica actually decrease duration time. Since the findings were done during an unsupervised time on each participants own computers perhaps conducting the experiment in lab settings would control for variability. Also we could tell participants that they will be given a monetary award if they answer the questions regarding the reading passage. This way with the given incentive it would be interesting to see if Sans Forgetica still takes up less time overall than other fonts. Additionally, it might also be interesting to do a repeated measures test to see the duration time for passages of similar difficulty in different fonts for the same individual. 

## References
Halin, N., Marsh, J.E., Hellman, A., Hellstrom, I., and P. Sorqvist. 2014. A shield against distraction. Journal of Applied Research in Memory and Cognition 3: 31-36. Doi: 10.1016/j.jarmac.2014.01.003

Eitel, A. and T. Kühl. 2016. Effects of disfluency and test expectancy on learning with text. Metacognition Learning 11:107-121. Doi: 10.1007/s11409-015-9145-3

Wery, J.J., and J.A. Diliberto. 2017. The effect of a specialized dyslexia font, OpenDyslexic, on reading rate and accuracy. Annals of Dyslexia 67: 114 – 127. Doi: 10.1007/s11881-016-0127-1



